<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form đăng ký sinh viên</title>
    <link rel="stylesheet" type="text/css" href="main.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        function updateDistricts() {
            var selectedCity = document.getElementById("thanh_pho").value;
            var quanSelect = document.getElementById("quan");
            quanSelect.innerHTML = ""; // Xóa các quận hiện tại

            if (selectedCity === "Hà Nội") {
                var hanoiDistricts = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
                for (var i = 0; i < hanoiDistricts.length; i++) {
                    var option = document.createElement("option");
                    option.text = hanoiDistricts[i];
                    quanSelect.add(option);
                }
            } else if (selectedCity === "TP.HCM") {
                var hcmDistricts = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
                for (var i = 0; i < hcmDistricts.length; i++) {
                    var option = document.createElement("option");
                    option.text = hcmDistricts[i];
                    quanSelect.add(option);
                }
            }
        }
    </script>
</head>
<body>
	<p>Form đăng ký sinh viên</p>
    <div id="errorMessages" class="error-messages"></div>
    <table border="1" width="700" height="400">
        	<tr>
                <th class="name" >Họ và tên</th>
                <td>
                	<input class=" fl-1" type="text" id="name" name="name" required>
                </td>
            </tr>
            
            <tr>
                <th class="gender">Giới tính</th>
                <td>
                	<input type="radio" id="male" name="gender" value="Nam" required> Nam
                    <input type="radio" id="female" name="gender" value="Nữ" required> Nữ


                </td>
            </tr>
            
            <tr>
                <th class="birth">Ngày sinh</th>
                <td>
                	Năm
                	<select name="nam_sinh" id="nam_sinh" required>
                    <?php
                    $currentYear = date("Y");
                    for ($year = $currentYear - 40; $year <= $currentYear - 15; $year++) {
                        echo '<option value="">' . $year . '</option>';
                    }
                    ?>
                </select>
                Tháng
                <select name="thang_sinh" id="thang_sinh" required>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                        echo "<option value=''>$i</option>";
                    }
                    ?>
                </select>
                Ngày
                <select name="ngay_sinh" id="ngay_sinh" required>
                    <?php
                    for ($i = 1; $i <= 31; $i++) {
                        echo "<option value=''>$i</option>";
                    }
                    ?>
                </select>


                </td>
            </tr>
            
            <tr>
                <th class="address">Địa chỉ</th>
                <td>
                	Thành phố
                	<select name="thanh_pho" id="thanh_pho" onchange="updateDistricts();" required>
                      <option value=""></option>
                      <option value="Hà Nội">Hà Nội</option>
                      <option value="TP.HCM">TP.HCM</option>
                	</select>

                	Quận
                	<select name="quan" id="quan">
                 		<option value=""></option>
                	</select>


                </td>
            </tr>
            
            <tr>
                <th class="other-infor">Thông tin khác</th>
                <td>
                	<input class="" name="infor" id="infor"></input>

                </td>
            </tr>
    </table>
	<br><br>
    <div class="button-container" id="registerButton">
        <button type="button">Đăng ký</button>
    </div>


</body>

<script>
    $(document).ready(function () {
        $("#registerButton button").click(function () {
            var name = $("#name").val();
            var gender = $("input[name='gender']:checked").val();
            var birthdate = $("#nam_sinh").val() + '-' + $("#thang_sinh").val() + '-' + $("#ngay_sinh").val();
            var address = $("#quan").val();

            var flag = true;
            var errorMessages = [];

            if (name === "") {
                errorMessages.push("Hãy nhập tên.");
                flag = false;
            }

            if (!gender) {
                errorMessages.push("Hãy chọn giới tính.");
                flag = false;
            }

            if (birthdate === "--") {
                errorMessages.push("Hãy chọn ngày sinh.");
                flag = false;
            }

            if (address === "") {
                errorMessages.push("Hãy chọn địa chỉ.");
                flag = false;
            }

            if (errorMessages.length > 0) {
                var errorMessageHtml = "";
                for (var i = 0; i < errorMessages.length; i++) {
                    errorMessageHtml += errorMessages[i] + "<br>";
                }
                $("#errorMessages").html(errorMessageHtml);
            } else {
                // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
                $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
            }
        });
    });
</script>

</html>
