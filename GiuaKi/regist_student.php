<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin đăng ký sinh viên</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Lấy dữ liệu từ biểu mẫu
        $name = $_POST["name"];
        $gender = $_POST["gender"];
        $birthdate = $_POST["ngay_sinh"] . '-' . $_POST["thang_sinh"] . '-' . $_POST["nam_sinh"];
        $address = $_POST["quan"];
        $infor = $_POST["infor"];
    ?>

    <p>Thông tin đăng ký</p>
    <table border="1" width="700" height="400">
        <tr>
            <th class="name">Họ và tên</th>
            <td><?= $name ?></td>
        </tr>
        <tr>
            <th class="gender">Giới tính</th>
            <td><?= $gender ?></td>
        </tr>
        <tr>
            <th class="birth">Ngày sinh</th>
            <td><?= $birthdate ?></td>
        </tr>
        <tr>
            <th class="address">Địa chỉ</th>
            <td><?= $address ?></td>
        </tr>
        <tr>
            <th class="other-infor">Thông tin khác</th>
            <td><?= $infor ?></td>
        </tr>
    </table>

    <?php
    } else {
        // Xử lý nếu không có dữ liệu POST
        echo "Dữ liệu không hợp lệ.";
    }
    ?>
</body>
</html>
