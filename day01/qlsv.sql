--5.1
CREATE DATABASE QLSV;
USE QLSV;

CREATE TABLE DMKHOA (
    MaKH varchar(6) NOT NULL PRIMARY KEY,
    TenKhoa varchar(30) NOT NULL
);

CREATE TABLE SINHVIEN (
    MaSV varchar(6) NOT NULL PRIMARY KEY,
    HoSV varchar(30) NOT NULL,
    TenSV varchar(15) NOT NULL,
    GioiTinh char(1),
    NgaySinh DateTime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong Int,x
    FOREIGN KEY (MaKH) REFERENCES DMKHOA(MaKH)
);
