<!DOCTYPE html>
<html lang="vi">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Đăng nhập</title>
	<style>
		/* CSS để cải thiện giao diện của trang đăng nhập */
		body {
			font-family: Arial, sans-serif;
			background-color: #f7f7f7;
			height: 100vh;
		}

		h1 {
			color: #333;
			text-align: center;
			margin: auto;
		}

		time {
			background-color: #383838;
		}


		form {
			background-color: #fff;
			padding: 20px;
			border-radius: 5px;
			box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
			text-align: center;
			max-width: 30%;
			width: 90%;
			justify-content: center;
    		align-items: center;
		}

		label {
			font-weight: bold;
			width: 50px;
			border: 1px solid #007bff;
			padding: 10px;
			margin: 8px 0;
			background-color: #81beff;
		}

		input[type="text"]{
			width: 50%;
			padding: 10px;
			margin: 8px 0;
			border: 1px solid #007bff;
		}
		input[type="password"] {
			width: 50%;
			padding: 10px;
			margin: 8px 0;
			border: 1px solid #007bff;
		}

		input[type="submit"] {
			border: 1px solid #007bff;
			background-color: #81beff;
			padding: 12px 30px;
			border-radius: 6px;
			cursor: pointer;
			font-weight: bold;
			margin-top: 10px;
			text-align: center;
			align-self: center;
		}
	</style>
</head>
<body>
	<div>
		<h1>Đăng nhập</h1>
		<form action="login.php" method="POST">
			<p><?php
				date_default_timezone_set('Asia/Ho_Chi_Minh'); // Đặt múi giờ là múi giờ của Việt Nam
				
				$ngay_hien_tai = date("d/m/Y");
				$gio_phut_hien_tai = date("H:i");
				$thu_hien_tai = strftime("%A", strtotime($ngay_hien_tai));
				
				echo "Bây giờ là: $gio_phut_hien_tai, thứ $thu_hien_tai ngày $ngay_hien_tai";
				?>
			</p>


			<label for="username">Tên đăng nhập</label>
			<input type="text" id="username" name="username" required><br><br>

			<label for="password">Mật khẩu</label>
			<input type="password" id="password" name="password" required><br><br>

			<input type="submit" value="Đăng nhập" >
		</form>


	</div>
</body>
</html>
