<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./style.css">
    <title>Đăng ký Tân sinh viên</title>
</head>

<body>
    <form action="" class="bd-blue">
        <table class="w-100">
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="name">Họ và tên</label></td>
                <td class="bd-blue">
                    <input type="text" id="name" name="name" required class="w-100 p-10-20 h-100">
                </td>
            </tr>
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="gender">Giới tính</label></td>
                <td>
                    <div class="d-flex">
                        <label class="gender-label" for="nam">Nam</label>
                        <div class="genders">
                            <input id="nam" type="radio" name="gender" value="Nam">
                        </div>
                        <label class="gender-label" for="nu">Nữ</label>
                        <div class="genders">
                            <input id="nu" type="radio" name="gender" value="Nữ">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="department">Phân khoa</label></td>
                <td>
                    <select id="department" name="department" class="bd-blue py-10">
                        <option value="">--Chọn phân khoa--</option>
                        <?php
                        $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                        foreach ($departments as $key => $value) {
                            echo "<option value=\"$key\">$value</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
        </table>
        <div class="text-center">
            <button type="submit" class="btn bd-blue bg-light-green text-white mt-20">Đăng ký</button>
        </div>
    </form>
</body>

</html>
