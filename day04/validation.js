$(document).ready(function () {
    $("#registerButton").click(function () {
        var name = $("#name").val();
        var gender = $("input[name='gender']:checked").val();
        var department = $("#department").val();
        var birthdate = $("#birthdate").val();

        var errorMessages = [];

        if (name === "") {
            errorMessages.push("Hãy nhập tên.");
        }

        if (!gender) {
            errorMessages.push("Hãy chọn giới tính.");
        }

        if (department === "") {
            errorMessages.push("Hãy chọn phân khoa.");
        }

        var datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
        if (birthdate === "") {
            errorMessages.push("Hãy nhập ngày sinh.");
        } else if (!datePattern.test(birthdate)) {
            errorMessages.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
        } else {
            // Kiểm tra tính hợp lệ của ngày tháng năm
            var parts = birthdate.split("/");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);

            if (isNaN(day) || isNaN(month) || isNaN(year) || day < 1 || day > 31 || month < 1 || month > 12 || year < 1900 || year > 2099) {
                errorMessages.push("Hãy nhập ngày sinh hợp lệ.");
            }
        }

        if (errorMessages.length > 0) {
            var errorMessageHtml = "";
            for (var i = 0; i < errorMessages.length; i++) {
                errorMessageHtml += errorMessages[i] + "<br>";
            }
            $("#errorMessages").html(errorMessageHtml);
        } else {
            // Nếu không có lỗi, thực hiện đăng ký hoặc xử lý form ở đây
            $("#errorMessages").html(""); // Xóa thông báo lỗi cũ
        }
    });
});
