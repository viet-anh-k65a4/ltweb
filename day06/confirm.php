<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css"> <!-- Liên kết đến tệp CSS để tùy chỉnh kiểu dáng. -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> <!-- Liên kết đến thư viện jQuery. -->
</head>

<?php
$departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu'); // Mảng các phòng ban.
?>

<body>
    <div class="container">
        <form id="registrationForm" class="bd-blue" method="POST" enctype="multipart/form-data">
            <!-- Mở form đăng ký với phương thức POST và cho phép tải lên tệp tin. -->
        <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20" for="name">Họ và
                    tên</div>
                <div class="fl-1 p-10-20">
                    <?php
                    echo $_POST['name']; // Hiển thị giá trị tên từ dữ liệu gửi đi (nếu có).
                    ?>
                </div>
        </div>

        <!-- (Tương tự cho các trường dữ liệu khác) -->

            <div class="form-group" style="align-items: unset">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center me-20"
                    for="profileImage" style="height: 20%">Hình ảnh</div>
                <div class="p-0-20 w-30">
                <?php
                if (isset($_FILES['profileImage'])) { // Kiểm tra xem có tệp hình ảnh được tải lên không.
                    if ($_FILES['profileImage']['error'] == 0) { // Kiểm tra lỗi khi tải lên (0 có nghĩa là không có lỗi).
                        $target_dir = 'uploads/';
                        $target_file = $target_dir . basename($_FILES['profileImage']['name']);
                        $uploads = true;
                        $maxFileSize = 84031 * 2; // Kích thước tối đa cho tệp hình ảnh (được xác định trước).
                        $fileType = array('jpg', 'png', 'jpeg', 'gif'); // Các loại tệp hình ảnh cho phép.
                        $isImageFileType = pathinfo($target_file, PATHINFO_EXTENSION); // Lấy định dạng của tệp.
                        if (file_exists($target_file)) {
                            echo '<div>File hình ảnh đã tồn tại</div>'; // Thông báo nếu tệp đã tồn tại.
                            $uploads = false;
                        }

                        if ($_FILES['profileImage']['size'] > $maxFileSize) {
                            echo '<div>Kích thước của hình ảnh quá lớn</div>'; // Thông báo nếu kích thước quá lớn.
                            $uploads = false;
                        }

                        if (!in_array($isImageFileType, $fileType)) {
                            echo '<div>Sai định dạng hình ảnh</div>'; // Thông báo nếu định dạng không hợp lệ.
                            $uploads = false;
                        }
                        if ($uploads) { // Nếu không có lỗi và tất cả kiểm tra đều đúng.
                            if (move_uploaded_file($_FILES['profileImage']['tmp_name'], $target_file)) {
                                echo '<img src="./uploads/' . $_FILES["profileImage"]['name'] . '" alt="" style="width:100%">';
                                // Hiển thị hình ảnh nếu tải lên thành công.
                            } else {
                                echo 'Không thể tải hình ảnh lên'; // Thông báo nếu không thể tải lên.
                            }
                        }
                    }
                }
                ?>
                </div>
            </div>

            <div class="button-container" id="registerButton">
                <button type="submit">Xác nhận</button>
                <!-- Nút gửi để xác nhận việc đăng ký. -->
            </div>
        </form>
    </div>
</body>

</html>
